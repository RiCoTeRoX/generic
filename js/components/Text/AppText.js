import React from 'react';
import { Text } from 'react-native';

const AppText = ({children}) => <Text>{children}</Text>;

export { AppText };
