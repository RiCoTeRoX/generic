import { StatusBar } from 'react-native';
import { Navigation } from 'react-native-navigation';
import Stories from './stories/Stories';
import Story from './story/Story';
import UI from '../stores/UI';
import prettyNumber from 'utils/prettyNumber';
import { getVar } from 'styles';
import { when } from 'mobx';

export const STORIES_SCREEN = 'hekla.StoriesScreen';
export const STORY_SCREEN = 'hekla.StoryScreen';

export const Screens = new Map();
Screens.set(STORIES_SCREEN, Stories);
Screens.set(STORY_SCREEN, Story);

export const startApp = () => {
  StatusBar.setBarStyle('dark-content', true);
  const isSplitView = UI.isIpad && UI.settings.appearance.iPadSidebarEnabled;

  const iconColor = getVar('--tabbar-fg');
  const textColor = getVar('--tabbar-fg');
  const selectedIconColor = getVar('--tabbar-tint');
  const selectedTextColor = getVar('--tabbar-tint');

  const tabs = [
    {
      stack: {
        id: 'STORY_SCREEN',
        children: isSplitView ? [{
          component: {
            name: IPAD_SCREEN,
          },
        }] : [{
          component: {
            name: STORIES_SCREEN,
          },
        }],
        options: {
          bottomTab: {
            iconColor,
            textColor,
            selectedIconColor,
            selectedTextColor,
            text: 'Stories',
            testID: 'STORIES_TAB',
            icon: require('assets/icons/25/stories.png'),
          },
        },
      },
    },
    {
      stack: {
        children: [{
          component: {
            name: ACCOUNT_SCREEN,
          },
        }],
        options: {
          bottomTab: {
            iconColor,
            textColor,
            selectedIconColor,
            selectedTextColor,
            text: 'Account',
            testID: 'ACCOUNT_TAB',
            icon: require('assets/icons/25/user.png'),
          },
        },
      },
    }, {
      stack: {
        children: [{
          component: {
            name: SEARCH_SCREEN,
          },
        }],
        options: {
          bottomTab: {
            iconColor,
            textColor,
            selectedIconColor,
            selectedTextColor,
            text: 'Search',
            testID: 'SEARCH_TAB',
            icon: require('assets/icons/25/search.png'),
          },
        },
      },
    }, {
      stack: {
        children: [{
          component: {
            name: SETTINGS_SCREEN,
          },
        }],
        options: {
          bottomTab: {
            iconColor,
            textColor,
            selectedIconColor,
            selectedTextColor,
            text: 'Settings',
            testID: 'SETTINGS_TAB',
            icon: require('assets/icons/25/settings.png'),
          },
        },
      },
    },
  ];

  if (isSplitView) {
    return Navigation.setRoot({
      root: {
        splitView: {
          id: 'SPLIT_VIEW',
          master: {
            stack: {
              id: 'MASTER_ID',
              children: [
                {
                  component: {
                    name: STORIES_SCREEN,
                  },
                },
              ],
            },
          },
          detail: {
            bottomTabs: {
              children: tabs,
            },
          },
          options: {
            displayMode: 'visible',
          },
        },
      },
    });
  }

  return Navigation.setRoot({
    root: {
      bottomTabs: {
        id: 'ROOT',
        children: tabs,
      },
    },
  });
};

export const replyScreen = (itemId: string, edit: boolean = false) => Navigation.showModal({
  stack: {
    children: [
      {
        component: {
          name: REPLY_SCREEN,
          passProps: {
            itemId,
            edit,
          },
        },
      },
    ],
  },
});

export const userScreen = (id: string) => Navigation.push(UI.componentId, {
  component: {
    name: USER_SCREEN,
    passProps: {
      id,
    },
    options: {
      topBar: {
        title: {
          text: id,
        },
      },
    },
  },
});

export const userSubmissionsScreen = (userId: string) => Navigation.push(UI.componentId, {
  component: {
    name: USER_SUBMISSIONS_SCREEN,
    passProps: {
      userId,
    },
  },
});

export const userCommentsScreen = (userId: string) => Navigation.push(UI.componentId, {
  component: {
    name: USER_COMMENTS_SCREEN,
    passProps: {
      userId,
    },
  },
});

export const userFavoritesScreen = (userId: string) => Navigation.push(UI.componentId, {
  component: {
    name: USER_FAVORITES_SCREEN,
    passProps: {
      userId,
    },
  },
});

export const accountHiddenScreen = () => Navigation.push(UI.componentId, {
  component: {
    name: ACCOUNT_HIDDEN_SCREEN,
  },
});

export const accountVotedScreen = (userId: string) => Navigation.push(UI.componentId, {
  component: {
    name: ACCOUNT_VOTED_SCREEN,
    passProps: {
      userId,
    },
  },
});

export const storyScreen = async (props: StoryScreenProps) => {
  const isSplitView = UI.isIpad && UI.settings.appearance.iPadSidebarEnabled;
  const { id, descendants = 0, isMasterView = false, reactTag } = props;

  const opts = {
    component: {
      name: STORY_SCREEN,
      passProps: {
        id: String(id),
      },
      options: {
        topBar: {
          title: {
            text: prettyNumber(descendants, 'Comments'),
          },
        },
        preview: reactTag ? {
          reactTag,
          commit: true,
        } : undefined,
      },
    },
  } as any;

  if (isSplitView && isMasterView) {
    // Pop to root
    await Navigation.popToRoot(UI.iPadDetailComponentId);
    // Switch to first tab
    await Navigation.mergeOptions(UI.iPadDetailComponentId, { bottomTabs: { currentTabIndex: 0 } });
    return when(
      // Wait for detail view to appear
      () => UI.componentId === UI.iPadDetailComponentId,
      // Then push new controller
      () => Navigation.push(UI.iPadDetailComponentId, opts),
    );
  }

  return Navigation.push(UI.componentId, opts);
};
