import React, { PureComponent } from 'react';
import { View, Image, Button } from 'react-native';

class Login extends PureComponent {
  static get options() {
    return {
      topBar: {
        drawBehind: false,
        visible: false,
        animate: false,
        title: {
          text: 'algos'
        }
      }
    };
  }

  render() {
    return (
      <View style={{ backgroundColor: '#ddd', flex: 1, paddingTop: 20 }}>
        <Image source={require('./../../assets/screens/login.png')} />
      </View>
    );
  }
}

export default Login;
