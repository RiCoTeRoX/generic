import React, { PureComponent } from 'react';
import { View, Image } from 'react-native';

class Dashboard extends PureComponent {
  static get options() {
    return {
      topBar: {
        drawBehind: false,
        visible: false,
        animate: false,
        title: {
          text: 'algos'
        }
      }
    };
  }

  render() {
    return (
      <View style={{ backgroundColor: '#ddd', flex: 1, paddingTop: 20 }}>
        <Image source={require('./../../assets/screens/dashboard.png')} />
      </View>
    );
  }
}

export default Dashboard;
