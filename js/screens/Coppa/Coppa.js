import React, { PureComponent } from 'react';
import { View, Image } from 'react-native';

class Coppa extends PureComponent {
  static get options() {
    return {
      topBar: {
        drawBehind: false,
        visible: false,
        animate: false,
        title: {
          text: 'algos'
        }
      }
    };
  }

  onConfirmCoppa(event) {
    console.log(event.targt, ' <-', this);
  }

  render() {
    return (
      <View style={{ backgroundColor: '#ddd', flex: 1, paddingTop: 20 }}>
        <Image source={require('./../../assets/screens/coppa.png')} />
      </View>
    );
  }
}

export default Coppa;
