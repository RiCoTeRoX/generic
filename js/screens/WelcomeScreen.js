const React = require('react');
const { Component } = require('react');
const { View, Text, Button, Platform } = require('react-native');

const testIDs = require('../testIDs');

const { Navigation } = require('react-native-navigation');

class WelcomeScreen extends Component {
  static get options() {
    return {
      _statusBar: {
        backgroundColor: 'transparent',
        style: 'dark',
        drawBehind: true
      },
      topBar: {
        title: {
          largeTitle: false,
          title: 'My Screen'
        },
        drawBehind: true,
        visible: false,
        animate: false
      }
    };
  }

  render() {
    return (
      <View style={styles.bar}>
        <View
          style={{
            width: 2,
            height: 2,
            borderRadius: 1,
            backgroundColor: 'red',
            alignSelf: 'center'
          }}
        />
        <View style={styles.root} key="root">
          <Text testID={testIDs.WELCOME_SCREEN_HEADER} style={styles.h1}>
            React Native Navigation!
          </Text>
          <Button
            title="Switch to tab based app"
            testID={testIDs.TAB_BASED_APP_BUTTON}
            onPress={this.onClickSwitchToTabs}
          />

          <Button
            title="Show Modal"
            testID={testIDs.SHOW_MODAL_BUTTON}
            onPress={this.onClickShowModal}
          />

          <Text style={styles.footer}>{`this.props.componentId = ${this.props.componentId}`}</Text>
        </View>
        <View
          style={{
            width: 2,
            height: 2,
            borderRadius: 1,
            backgroundColor: 'red',
            alignSelf: 'center'
          }}
        />
      </View>
    );
  }

  onClickSwitchToTabs = () => {
    Navigation.setRoot({
      root: {
        bottomTabs: {
          children: [
            {
              stack: {
                id: 'TAB1_ID',
                children: [
                  {
                    component: {
                      name: 'navigation.playground.TextScreen',
                      passProps: {
                        text: 'This is tab 1',
                        myFunction: () => 'Hello from a function!'
                      },
                      options: {
                        topBar: {
                          visible: true,
                          title: {
                            text: 'React Native Navigation!'
                          }
                        }
                      }
                    }
                  }
                ],
                options: {
                  bottomTab: {
                    title: 'Tab 1',
                    icon: require('../images/one.png'),
                    testID: testIDs.FIRST_TAB_BAR_BUTTON
                  },
                  topBar: {
                    visible: false
                  }
                }
              }
            },
            {
              stack: {
                children: [
                  {
                    component: {
                      name: 'navigation.playground.TextScreen',
                      passProps: {
                        text: 'This is tab 2'
                      }
                    }
                  }
                ],
                options: {
                  bottomTab: {
                    title: 'Tab 2',
                    icon: require('../images/two.png'),
                    testID: testIDs.SECOND_TAB_BAR_BUTTON
                  }
                }
              }
            }
          ],
          options: {
            bottomTabs: {
              tabColor: 'red',
              selectedTabColor: 'blue',
              fontFamily: 'HelveticaNeue-Italic',
              fontSize: 13,
              testID: testIDs.BOTTOM_TABS_ELEMENT
            }
          }
        }
      }
    });
  };

  onClickShowModal = async () => {
    await Navigation.showModal({
      stack: {
        children: [
          {
            component: {
              name: 'navigation.playground.ModalScreen'
            }
          }
        ]
      }
    });
  };
}

module.exports = WelcomeScreen;

const styles = {
  root: {
    flexGrow: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#e8e8e8'
  },
  bar: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#e8e8e8',
    justifyContent: 'space-between'
  },
  h1: {
    fontSize: 24,
    textAlign: 'center',
    margin: 30
  },
  footer: {
    fontSize: 10,
    color: '#888',
    marginTop: 10
  }
};
