#!/usr/bin/env bash

#Replace script for integration

echo "Start replace script"

echo "Replace in RCTNativeAnimatedNodesManager.h"
sed -i '' "s/#import <RCTAnimation\\/RCTValueAnimatedNode.h>/#import <React\\/RCTValueAnimatedNode.h>/" ./node_modules/react-native/Libraries/NativeAnimation/RCTNativeAnimatedNodesManager.h

echo "Replace ReactNativeNavigation.podspec"
cp -f ./NavigationEter ./node_modules/react-native-navigation/ReactNativeNavigation.podspec

echo "Finish replace"
