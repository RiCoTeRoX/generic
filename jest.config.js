module.exports = {
  preset: 'react-native',
  transform: {
    '.(js)': 'babel-jest',
    '.(ts|tsx)': 'ts-jest'
  },
  testRegex: '(/__tests__/.*|\\.(test|spec))\\.(ts|tsx|js)$',
  moduleFileExtensions: ['ts', 'js', 'tsx'],
  moduleNameMapper: {
    '^[./a-zA-Z0-9$_-]+\\.(bmp|gif|jpg|jpeg|png|psd|svg|webp)$': 'RelativeImageStub',
    '^[./a-zA-Z0-9$_-]+\\.(ttf|m4v|mov|mp4|mpeg|mpg|webm|aac|aiff|caf|m4a|mp3|wav|html|pdf|obj)$':
      'RelativeImageStub',
    '@assets/images': '<rootDir>/src/assets/images'
  },
  transformIgnorePatterns: ['./node_modules/(?!(jest-)?react-native|glamorous-native)'],
  snapshotSerializers: ['enzyme-to-json/serializer'],
  setupTestFrameworkScriptFile: './node_modules/jest-enzyme/lib/index.js',
  testPathIgnorePatterns: ['./dist/', './node_modules/'],
  globals: {
    'ts-jest': {
      useBabelrc: true
    }
  }
};
